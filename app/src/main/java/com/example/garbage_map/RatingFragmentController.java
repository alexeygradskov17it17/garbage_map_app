package com.example.garbage_map;

import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.example.garbage_map.dao.AccountsDAO;
import com.example.garbage_map.entities.Account;

import java.util.List;

public class RatingFragmentController extends Fragment {
    AppDatabase database;
    AccountsDAO aDAO;
    RecyclerView recyclerView;
    TextView textView;


    public RatingFragmentController() {
    }

    public static RatingFragmentController newInstance() {
        return new RatingFragmentController();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.rank_fragment, container, false);
        database = Room.databaseBuilder(getContext(), AppDatabase.class, "mydb")
                .allowMainThreadQueries()
                .build();
        aDAO = database.getAccountDAO();
        loadUI(view);
        return view;
    }

    public void loadUI(View view) {
        recyclerView = view.findViewById(R.id.ranksRV);
        textView = view.findViewById(R.id.rankLabelTV);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayWidth = size.x;
        int displayHeight = size.y;

        textView.setY((float) (displayHeight*0.01));
        textView.setX((float) (displayWidth*0.02));

        recyclerView.setX((float) (displayWidth*0.02));
        recyclerView.setY((float) (displayHeight*0.06));

        List<Account> accounts = aDAO.getOrderedScores();
        CustomAdapter customAdapter = new CustomAdapter(view.getContext(), accounts);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(customAdapter);
    }
}

