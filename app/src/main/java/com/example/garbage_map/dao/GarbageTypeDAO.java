package com.example.garbage_map.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Update;

import com.example.garbage_map.entities.GarbageType;

@Dao
public interface GarbageTypeDAO {
    @Insert
    public void insert(GarbageType... garbageTypes);
    @Update
    public void update(GarbageType... garbageTypes);
    @Delete
    public void delete(GarbageType garbageType);
}
