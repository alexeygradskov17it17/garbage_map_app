package com.example.garbage_map.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;


import com.example.garbage_map.entities.RecyclingCollectionPoint;

import java.util.List;

@Dao
public interface RecyclingPointDAO {
    @Insert
    public void insert(RecyclingCollectionPoint... recyclingCollectionPoints);

    @Update
    public void update(RecyclingCollectionPoint... recyclingCollectionPoints);

    @Delete
    public void delete(RecyclingCollectionPoint recyclingCollectionPoint);

    @Query("SELECT * FROM recycling_points WHERE idOfType = :id")
    public List<RecyclingCollectionPoint> getPointsByType(int id);

    @Query("SELECT * FROM recycling_points")
    public List<RecyclingCollectionPoint> getAllPoints();
}
