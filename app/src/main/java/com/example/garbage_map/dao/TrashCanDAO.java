package com.example.garbage_map.dao;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.garbage_map.entities.GarbageType;
import com.example.garbage_map.entities.TrashCan;

import java.util.List;

@Dao
public
interface TrashCanDAO {
    @Insert
    public void insert(TrashCan... trashCans);

    @Update
    public void update(TrashCan... trashCans);

    @Delete
    public void delete(TrashCan trashCan);

    @Query("SELECT * FROM trashcan WHERE idOfType = :id")
    public List<TrashCan> getCansByType(int id);

    @Query("SELECT * FROM trashcan")
    public List<TrashCan> getAllCans();
}

