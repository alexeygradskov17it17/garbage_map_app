package com.example.garbage_map.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.garbage_map.entities.Account;
import com.example.garbage_map.entities.GarbageType;

import java.util.List;

@Dao
public interface AccountsDAO{
    @Insert
    public void insert(Account... accounts);
    @Update
    public void update(Account... accounts);
    @Delete
    public void delete(Account account);

    @Query("SELECT * FROM accounts")
    public List<Account> getAllAccounts();

    @Query("SELECT * FROM accounts ORDER BY score")
    public List<Account> getOrderedScores();

    @Query("SELECT * FROM accounts WHERE username= :username")
    public List<Account> getAccountsByLogin(String username);

}

