package com.example.garbage_map;

import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.stream.Collectors;

public class MainActivity extends FragmentActivity implements CONSTANTS {
    SharedPreferences sharedPreferences;

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
        if (item.getItemId() == R.id.navigation_home) {
            System.out.println("Загружен фрагмент с картой");
            loadFragment(MapFragment.newInstance());
            return true;
        }
        if (item.getItemId() == R.id.navigation_dashboard) {
            System.out.println("Загружен фрагмент с новостями");
            loadFragment(NewsFragmentController.newInstance());
            return true;
        }
        if (item.getItemId() == R.id.navigation_notifications) {
            if (sharedPreferences.getBoolean(KEY_PREFS_IS_AUTH, false)) {
                System.out.println("Загружен фрагмент с личным кабинетом пользователя");
                loadFragment(PersonalAccountFragmentController.newInstance());
            } else {
                System.out.println("Загружен фрагмент с авторизацией");
                loadFragment(AuthorizationFragmentController.newInstance());
            }
            return true;
        }
        return false;
    };

    /**
     * Заменяет текущий фрагмент на новый
     *
     * @param fragment фрагмент
     */

    private void loadFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_content, fragment);
        fragmentTransaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        sharedPreferences = getSharedPreferences(KEY_ACCOUNT_DATA, Context.MODE_PRIVATE);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.bndrawer);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.fl_content, new MapFragment());
        fragmentTransaction.commit();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    protected void onActivityResult(final int requestCode, final int resultCode,
                                    final Intent data) {
        if (requestCode == 999 && resultCode == RESULT_OK) {
            String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            System.out.println(accountName);
            System.out.println("Новый пароль: " + new Random().ints(10, 48, 122).mapToObj(i -> String.valueOf((char) i)).collect(Collectors.joining()));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}