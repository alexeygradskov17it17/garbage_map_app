package com.example.garbage_map;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.example.garbage_map.entities.News;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.AccountViewHolder> {
    private List<News> mNews = new ArrayList<>();
    private Context mContext;

    public NewsAdapter(Context context, List<News> news) {
        mContext = context;
        mNews = news;
    }

    @Override
    public NewsAdapter.AccountViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.news_item_list, parent, false);
        AccountViewHolder viewHolder = new AccountViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NewsAdapter.AccountViewHolder holder, int position) {
        holder.bindRestaurant(mNews.get(position));
        holder.itemView.setOnClickListener(view -> {
            if (mNews.get(position).getId()==1){
                FragmentManager fragmentManager = ((FragmentActivity)view.getContext()).getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fl_content, RatingFragmentController.newInstance());
                fragmentTransaction.commit();
            } else {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                mContext.startActivity(browserIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mNews.size();
    }


    public class AccountViewHolder extends RecyclerView.ViewHolder{
        TextView header, description;

        private Context mContext;

        public AccountViewHolder(View itemView) {
            super(itemView);
            mContext = itemView.getContext();
            header =itemView.findViewById(R.id.header);
            description = itemView.findViewById(R.id.description);
        }

        public void bindRestaurant(News news) {
            header.setText(news.getHeader());
            description.setText(news.getDescription());
        }

        }
    }



