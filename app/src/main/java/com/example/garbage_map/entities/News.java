package com.example.garbage_map.entities;

public class News {
    private int id;
    private String header;
    private String description;

    public News(int id, String header, String description){
        this.id = id;
        this.header = header;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
