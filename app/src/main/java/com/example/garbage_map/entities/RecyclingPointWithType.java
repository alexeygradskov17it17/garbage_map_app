package com.example.garbage_map.entities;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

class RecyclingPointWithType {
    @Embedded
    public GarbageType garbageType;
    @Relation(parentColumn = "id", entity = RecyclingCollectionPoint.class, entityColumn = "idOfType")
    public List<RecyclingCollectionPoint> recyclingCollectionPoints;
}
