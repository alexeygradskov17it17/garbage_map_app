package com.example.garbage_map.entities;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "recycling_points", foreignKeys = @ForeignKey(entity = GarbageType.class,
        parentColumns = "id",
        childColumns = "idOfType",
        onDelete = ForeignKey.CASCADE))
public class RecyclingCollectionPoint {
    private @PrimaryKey
    int id;
    private int idOfType;
    private String address;
    private double latitude;
    private double longitude;

    public RecyclingCollectionPoint(int id, int idOfType,
                                    String address, double latitude,
                                    double longitude) {
        this.id = id;
        this.idOfType = idOfType;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdOfType() {
        return idOfType;
    }

    public void setIdOfType(int idOfType) {
        this.idOfType = idOfType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
