package com.example.garbage_map.entities;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;

@Entity(tableName = "trashcan",
        foreignKeys = @ForeignKey(entity = GarbageType.class,
                parentColumns = "id",
                childColumns = "idOfType",
                onDelete = ForeignKey.CASCADE))
public class TrashCan {
    @PrimaryKey
    private int id;
    private int idOfType;
    private String address;
    private double latitude;
    private double longitude;

    public TrashCan(int id, int idOfType,
                    String address, double latitude,
                    double longitude) {
        this.id = id;
        this.idOfType = idOfType;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdOfType() {
        return idOfType;
    }

    public void setIdOfType(int idOfType) {
        this.idOfType = idOfType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
