package com.example.garbage_map.entities;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

class TrashCanWithType {
    @Embedded
    public GarbageType garbageType;
    @Relation(parentColumn = "id", entity = TrashCan.class, entityColumn = "idOfType")
    public List<TrashCan> trashCans;
}

