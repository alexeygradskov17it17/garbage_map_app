package com.example.garbage_map;

interface CONSTANTS {
    int KEY_PLASTIC = 1;
    int KEY_GLASS = 3;
    int KEY_PAPER = 2;
    int KEY_METAL = 4;
    String KEY_PREFS_LOGIN = "login";
    String KEY_PREFS_IS_AUTH = "isAuth";
    String KEY_ACCOUNT_DATA = "acc_data";
}
