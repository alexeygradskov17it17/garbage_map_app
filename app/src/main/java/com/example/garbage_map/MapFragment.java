package com.example.garbage_map;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.room.Room;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.garbage_map.dao.GarbageTypeDAO;
import com.example.garbage_map.dao.RecyclingPointDAO;
import com.example.garbage_map.dao.TrashCanDAO;
import com.example.garbage_map.entities.GarbageType;
import com.example.garbage_map.entities.RecyclingCollectionPoint;
import com.example.garbage_map.entities.TrashCan;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MapFragment extends Fragment implements OnMapReadyCallback, CONSTANTS {

    public GoogleMap mMap;
    Polyline route;
    Location location;
    private FusedLocationProviderClient mFusedLocationClient;
    ArrayList<LatLng> points;
    private final Map<Marker, Map<String, Object>> markers = new HashMap<>();
    private String selectedType = "Все";
    private List<TrashCan> trashCans;
    private List<RecyclingCollectionPoint> recyclingCollectionPoints;

    private double wayLatitude = 0.0, wayLongitude = 0.0;
    private LocationRequest locationRequest;
    private LocationCallback locationCallback;
    private Marker selectedMarker;
    private AppDatabase database;
    private GarbageTypeDAO gTDAO;
    private TrashCanDAO tCDAO;
    private RecyclingPointDAO rpDAO;

    private boolean isContinue = false;
    private boolean isGPS = false;
    private Button btnCreateRoute, btnDeleteRoute;
    private Spinner spinner;

    public MapFragment() {
    }

    public static MapFragment newInstance() {
        return new MapFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.map_fragment,
                container, false);
        database = Room.databaseBuilder(getContext(), AppDatabase.class, "mydb")
                .allowMainThreadQueries()
                .build();
        gTDAO = database.getGarbageTypeDAO();
        tCDAO = database.getTrashCansDAO();
        rpDAO = database.getRecyclingPointDAO();
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        spinner = view.findViewById(R.id.spinner);
        loadUI(view);



        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(1000); // 10 seconds
        locationRequest.setFastestInterval(1000); // 5 seconds

        new MyLocationListener(getContext()).turnGPSOn(isGPSEnable -> {
            // turn on GPS
            isGPS = isGPSEnable;
        });

        mapFragment.getMapAsync(this);
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location currentLocation : locationResult.getLocations()) {
                    if (currentLocation != null) {
                        wayLatitude = currentLocation.getLatitude();
                        wayLongitude = currentLocation.getLongitude();
                        if (!isContinue) {
                            location = currentLocation;
                        } else {
                            location = currentLocation;
                            if (route != null) {
                                route.remove();
                            }
                            location = currentLocation;
                            LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());
                            String url = getDirectionsUrl(origin, new LatLng(selectedMarker.getPosition().latitude, selectedMarker.getPosition().longitude));
                            DownloadTask downloadTask = new DownloadTask();
                            downloadTask.execute(url);
                        }
                        if (!isContinue && mFusedLocationClient != null) {
                            mFusedLocationClient.removeLocationUpdates(locationCallback);
                        }
                    }
                }
            }
        };
        return view;
    }

    private void loadUI(View view) {
        btnCreateRoute = view.findViewById(R.id.btnCreateRoute);
        btnDeleteRoute = view.findViewById(R.id.btnDeleteRoute);
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        ArrayAdapter<?> adapter =
                ArrayAdapter.createFromResource(getContext(), R.array.types, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent,
                                       View itemSelected, int selectedItemPosition, long selectedId) {
                String[] choose = getResources().getStringArray(R.array.types);
                selectedType = choose[selectedItemPosition];
                getCans();
                mMap.clear();
                for (int i = 0; i < trashCans.size() - 1; i++) {
                    mMap.addMarker(new MarkerOptions().position(new LatLng(trashCans.get(i).getLatitude(), trashCans.get(i).getLongitude())));
                }
            }
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnCreateRoute.setLayoutParams(new FrameLayout.LayoutParams((int) (width * 0.5), (int) (height * 0.06)));
        btnCreateRoute.setX((float) ((width * 0.25)));
        btnCreateRoute.setY((float) (height * 0.15));
        btnDeleteRoute.setLayoutParams(new FrameLayout.LayoutParams((int) (width * 0.5), (int) (height * 0.06)));
        btnDeleteRoute.setX((float) ((width * 0.25)));
        btnDeleteRoute.setY((float) (height * 0.15));
        btnDeleteRoute.setOnClickListener(v -> {
            btnDeleteRoute.setVisibility(View.INVISIBLE);
            isContinue = false;
            route.remove();
        });

        btnCreateRoute.setOnClickListener(v -> {
            if (location == null) {
                Toast.makeText(getContext(), "Пожалуйста, подождите, определяем вашу геолокацию", Toast.LENGTH_SHORT).show();
                return;
            }
            isContinue = true;
            getLocation();
            LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());
            String url = getDirectionsUrl(origin, new LatLng(selectedMarker.getPosition().latitude, selectedMarker.getPosition().longitude));
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);
            System.out.println(isContinue + "**************************************");
            btnDeleteRoute.setVisibility(View.VISIBLE);
            btnCreateRoute.setVisibility(View.INVISIBLE);
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        System.out.println("Started");
        if (!isGPS) {
            Toast.makeText(getContext(), "Пожалуйста, включите GPS", Toast.LENGTH_LONG).show();
            return;
        }
        isContinue = false;
        getLocation();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        System.out.println("Destroyed");
    }

    @Override
    public void onPause() {
        super.onPause();
        System.out.println("Paused");
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    1000);
            Toast.makeText(getContext(), "Пожалуйста предоставльте доступ к геолокации", Toast.LENGTH_LONG).show();
        } else {
            if (isContinue) {
                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
            } else {
                mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), currentLocation -> {
                    if (currentLocation != null) {
                        location = currentLocation;
                        if (route != null) {
                            route.remove();
                            LatLng origin = new LatLng(location.getLatitude(), location.getLongitude());
                            String url = getDirectionsUrl(origin, new LatLng(selectedMarker.getPosition().latitude, selectedMarker.getPosition().longitude));
                            DownloadTask downloadTask = new DownloadTask();
                            downloadTask.execute(url);
                        }
                    } else {
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    }
                });
            }
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1000: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    if (isContinue) {
                        mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                    } else {
                        mFusedLocationClient.getLastLocation().addOnSuccessListener(getActivity(), currentLocation -> {
                            if (currentLocation != null) {
                                Toast.makeText(getContext(), "yes", Toast.LENGTH_LONG).show();
                            } else {
                                mFusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null);
                            }
                        });
                    }
                } else {
                    Toast.makeText(getContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                break;
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 1001) {
                isGPS = true; // flag maintain before get location
            }
        }
    }

    public void getCans() {
        switch (selectedType) {
            case "Все":
                trashCans = tCDAO.getAllCans();
                recyclingCollectionPoints = rpDAO.getAllPoints();
                break;
            case "Пластик":
                trashCans = tCDAO.getCansByType(1);
                recyclingCollectionPoints = rpDAO.getPointsByType(1);
                break;
            case "Бумага":
                trashCans = tCDAO.getCansByType(2);
                recyclingCollectionPoints = rpDAO.getPointsByType(2);
                break;
            case "Стекло":
                trashCans = tCDAO.getCansByType(3);
                recyclingCollectionPoints = rpDAO.getPointsByType(3);
                break;
            case "Металл":
                trashCans = tCDAO.getCansByType(4);
                recyclingCollectionPoints = rpDAO.getPointsByType(4);
                break;
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);

        googleMap.setOnMarkerClickListener(marker -> {
            btnCreateRoute.setVisibility(View.VISIBLE);
            selectedMarker = marker;
            return false;
        });
        MarkerOptions markerOptions = new MarkerOptions();
        LatLng latLng = new LatLng(53.199099, 44.988582);
        // Setting the position for the marker
        markerOptions.position(latLng);
        markerOptions.title(latLng.latitude + " : " + latLng.longitude);
        googleMap.clear();
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.addMarker(markerOptions);
        getCans();
        for (int i = 0; i < trashCans.size() - 1; i++) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(trashCans.get(i).getLatitude(), trashCans.get(i).getLongitude())));
        }
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(false);


    }


    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {
            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();
                List<HashMap<String, String>> path = result.get(i);
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);
                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }
                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.RED);
                lineOptions.geodesic(true);
            }
            route = mMap.addPolyline(lineOptions);
        }
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String mode = "mode=driving";
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&key=AIzaSyBHHJp2tpf6XF0Csrw1_iWGtgzkxSapP-k";
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;
        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }
}


