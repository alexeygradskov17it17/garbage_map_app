package com.example.garbage_map;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.garbage_map.entities.News;

import java.util.ArrayList;
import java.util.List;

public class NewsFragmentController extends Fragment {
    RecyclerView recyclerView;
    List<News> newsList;

    public NewsFragmentController() {
    }

    public static NewsFragmentController newInstance() {
        return new NewsFragmentController();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.news_fragment, container, false);
        newsList = new ArrayList<>();
        newsList.add(new News(1, "Призы за сдачу мусора в пункты приема!", "Сортируйте отходы, сдавайте их в пункты приема мусора, сканируйте чеки и получайте призы"));
        newsList.add(new News(2, "Почему так важно распределять отходы?", " Прочитайте статью о том, какие последствия несет рост количества свалок и почему так важно сортировать мусор перед утилизацией"));
        loadUI(view);
        return view;
    }

    private void loadUI(View v) {
        recyclerView = v.findViewById(R.id.newsRecyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(new NewsAdapter(v.getContext(), newsList));
    }
}
