package com.example.garbage_map;


import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.garbage_map.dao.AccountsDAO;
import com.example.garbage_map.dao.GarbageTypeDAO;
import com.example.garbage_map.dao.RecyclingPointDAO;
import com.example.garbage_map.dao.TrashCanDAO;
import com.example.garbage_map.entities.Account;
import com.example.garbage_map.entities.GarbageType;
import com.example.garbage_map.entities.RecyclingCollectionPoint;
import com.example.garbage_map.entities.TrashCan;

@Database(entities = {GarbageType.class, TrashCan.class, RecyclingCollectionPoint.class, Account.class}, version = 2)
public abstract class AppDatabase extends RoomDatabase {
    public abstract GarbageTypeDAO getGarbageTypeDAO();
    public abstract TrashCanDAO getTrashCansDAO();
    public abstract RecyclingPointDAO getRecyclingPointDAO();
    public abstract AccountsDAO getAccountDAO();
}
