package com.example.garbage_map;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.Room;

import com.example.garbage_map.dao.AccountsDAO;
import com.example.garbage_map.entities.Account;
import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Pattern;

public class RegistrationFragment extends Fragment {

    AccountsDAO aDAO;
    AppDatabase baseData;
    TextInputLayout loginTF, passwordTF;

    public RegistrationFragment(){}

    public static RegistrationFragment newInstance(){
        return new RegistrationFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.registration_fragment, container, false);
        baseData = Room.databaseBuilder(getContext(), AppDatabase.class, "mydb")
                .allowMainThreadQueries()
                .build();
         aDAO = baseData.getAccountDAO();

        loadUI(view);
        return view;
    }

    public void loadUI(View v) {
        loginTF = v.findViewById(R.id.loginTF);
        passwordTF = v.findViewById(R.id.passwordTFRegister);
        Button btnRegister = v.findViewById(R.id.btnRegister);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayWidth = size.x;
        int displayHeight = size.y;

        loginTF.setLayoutParams(new FrameLayout.LayoutParams((int) (displayWidth * 0.7), (int) (displayHeight * 0.08)));
        passwordTF.setLayoutParams(new FrameLayout.LayoutParams((int) (displayWidth * 0.7), (int) (displayHeight * 0.08)));
        btnRegister.setLayoutParams(new FrameLayout.LayoutParams((int) (displayWidth * 0.4), (int) (displayHeight * 0.06)));

        loginTF.setX((float) (displayWidth * 0.15));
        passwordTF.setX((float) (displayWidth * 0.15));
        btnRegister.setX((float) (displayWidth * 0.3));

        loginTF.setY((float) (displayHeight * 0.25));
        passwordTF.setY((float) (displayHeight * 0.4));
        btnRegister.setY((float) (displayHeight * 0.65));

        btnRegister.setOnClickListener(view -> {
            if (isValidForms()) {
                if (isValidPassword()) {
                    aDAO.insert(new Account(aDAO.getAllAccounts().size() + 1, loginTF.getEditText().getText().toString(), passwordTF.getEditText().getText().toString(), 0));
                } else Toast.makeText(getContext(),"Пароль должен содержать: \n -6 символов \n -1 латинскую букву нижнего регистра \n -1 латинскую букву верхнего регистра \n -1 спецсимвол", Toast.LENGTH_LONG).show();
            }
        });
    }

    public boolean isValidPassword() {
        return Pattern.matches("(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{6,}", passwordTF.getEditText().getText().toString());
    }

    public boolean isValidForms() {
        return !loginTF.getEditText().getText().toString().equals("") && !passwordTF.getEditText().getText().toString().equals("");
    }
}

