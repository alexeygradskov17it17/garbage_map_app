package com.example.garbage_map;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import com.example.garbage_map.dao.AccountsDAO;
import com.example.garbage_map.entities.Account;

import de.hdodenhof.circleimageview.CircleImageView;

public class PersonalAccountFragmentController extends Fragment implements CONSTANTS {
    CircleImageView imageView;
    TextView textView, scoreTV;
    private SharedPreferences sharedPreferences;
    AppDatabase database;
    AccountsDAO aDAO;
    Button btnOutFromAcc, btnScanCheck;


    public PersonalAccountFragmentController() {
    }

    public static PersonalAccountFragmentController newInstance() {
        return new PersonalAccountFragmentController();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_personal_account, container, false);
        database = Room.databaseBuilder(getContext(), AppDatabase.class, "mydb")
                .allowMainThreadQueries()
                .build();
        aDAO = database.getAccountDAO();
        sharedPreferences = view.getContext().getSharedPreferences(KEY_ACCOUNT_DATA, Context.MODE_PRIVATE);
        loadUI(view);
        return view;
    }

    public void loadUI(View view) {
        imageView = view.findViewById(R.id.avatarIMG);
        btnOutFromAcc = view.findViewById(R.id.outFromAccBtn);
        btnScanCheck = view.findViewById(R.id.scanCheckBtn);
        imageView.setImageResource(R.mipmap.ic_launcher_round);
        textView = view.findViewById(R.id.usernameTV);
        scoreTV = view.findViewById(R.id.scoreTV);
        Account account = aDAO.getAccountsByLogin(sharedPreferences.getString(KEY_PREFS_LOGIN, "")).get(0);
        textView.setText(account.getUsername());
        scoreTV.setText("Баллы: "+account.getScore());
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayWidth = size.x;
        int displayHeight = size.y;

        imageView.setLayoutParams(new FrameLayout.LayoutParams((int) (displayWidth * 0.2), (int) (displayHeight * 0.2)));
        imageView.setY((float) (displayHeight * 0.05));
        imageView.setX((float) (displayWidth * 0.05));

        textView.setTextSize((float) (displayWidth*0.02));
        textView.setY((float) (displayHeight * 0.11));
        textView.setX((float) (displayWidth * 0.3));

        btnOutFromAcc.setLayoutParams(new FrameLayout.LayoutParams((int) (displayWidth * 0.9), (int) (displayHeight * 0.06)));
        btnOutFromAcc.setY((float) (displayHeight * 0.8));
        btnOutFromAcc.setX((float) (displayWidth * 0.05));

        btnScanCheck.setLayoutParams(new FrameLayout.LayoutParams((int) (displayWidth * 0.9), (int) (displayHeight * 0.06)));
        btnScanCheck.setY((float) (displayHeight * 0.7));
        btnScanCheck.setX((float) (displayWidth * 0.05));

        scoreTV.setTextSize((float) (displayWidth*0.02));
        scoreTV.setY((float) (displayHeight * 0.15));
        scoreTV.setX((float) (displayWidth * 0.3));

        btnOutFromAcc.setOnClickListener(view1 -> {
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(KEY_PREFS_IS_AUTH, false);
            editor.putString(KEY_PREFS_LOGIN, "");
            editor.apply();
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fl_content, AuthorizationFragmentController.newInstance());
            fragmentTransaction.commit();
        });

    }
}
