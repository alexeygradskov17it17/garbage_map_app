package com.example.garbage_map;

import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.room.Room;

import com.example.garbage_map.dao.AccountsDAO;
import com.example.garbage_map.entities.Account;
import com.google.android.gms.common.AccountPicker;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

import static android.app.Activity.RESULT_OK;

public class AuthorizationFragmentController extends Fragment implements CONSTANTS {
    TextInputLayout loginTF, passwordTF;
    Button btnAuthorize, btnRegister;
    ImageView googleAuth;
    TextView textView;
    AppDatabase baseData;
    AccountsDAO aDAO;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    TextInputEditText editText;
    TextInputEditText editText1;


    public AuthorizationFragmentController() {
    }

    public static AuthorizationFragmentController newInstance() {
        return new AuthorizationFragmentController();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_authorize, container, false);
        baseData = Room.databaseBuilder(getContext(), AppDatabase.class, "mydb")
                .allowMainThreadQueries()
                .build();
        aDAO = baseData.getAccountDAO();
        sharedPreferences = view.getContext().getSharedPreferences(KEY_ACCOUNT_DATA, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

        loadUI(view);
        return view;
    }

    private void loadUI(View view) {
        loginTF = view.findViewById(R.id.authBtnLogin);
        passwordTF = view.findViewById(R.id.authBtnPass);
        btnAuthorize = view.findViewById(R.id.authBtnAuthorize);
        btnRegister = view.findViewById(R.id.authBtnRegister);
        googleAuth = view.findViewById(R.id.imageBtnGoogleAuth);
        textView = view.findViewById(R.id.tVAuthWith);
        editText = view.findViewById(R.id.loginET);
        editText1 = view.findViewById(R.id.edit_text2);

        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int displayWidth = size.x;
        int displayHeight = size.y;

        loginTF.setLayoutParams(new FrameLayout.LayoutParams((int) (displayWidth * 0.7), (int) (displayHeight * 0.08)));
        passwordTF.setLayoutParams(new FrameLayout.LayoutParams((int) (displayWidth * 0.7), (int) (displayHeight * 0.08)));
        btnAuthorize.setLayoutParams(new FrameLayout.LayoutParams((int) (displayWidth * 0.4), (int) (displayHeight * 0.06)));
        btnRegister.setLayoutParams(new FrameLayout.LayoutParams((int) (displayWidth * 0.4), (int) (displayHeight * 0.06)));
        googleAuth.setLayoutParams(new FrameLayout.LayoutParams((int) (displayWidth * 0.15), (int) (displayHeight * 0.15)));
        textView.setLayoutParams(new FrameLayout.LayoutParams((int) (displayWidth), (int) (displayHeight * 0.06)));

        loginTF.setX((float) (displayWidth * 0.15));
        passwordTF.setX((float) (displayWidth * 0.15));
        btnAuthorize.setX((float) (displayWidth * 0.3));
        btnRegister.setX((float) (displayWidth * 0.3));
        googleAuth.setX((float) (displayWidth * 0.4));
        textView.setX((float) (displayWidth * 0.25));


        loginTF.setY((float) (displayHeight * 0.25));
        passwordTF.setY((float) (displayHeight * 0.4));
        btnAuthorize.setY((float) (displayHeight * 0.55));
        btnRegister.setY((float) (displayHeight * 0.65));
        googleAuth.setY((float) (displayHeight * 0.76));
        textView.setY((float) (displayHeight * 0.75));
        googleAuth.setImageResource(R.drawable.google_icon);
        googleAuth.setClickable(true);
        List<Account> foundAccounts = aDAO.getAccountsByLogin(loginTF.toString());
        btnAuthorize.setOnClickListener(view1 -> {
            if (isValidForms()) {
                if (foundAccounts.size() > 0) {
                    if (foundAccounts.get(0).getPassword().equals(editText1.getText().toString())) {
                        editor.putString(KEY_PREFS_LOGIN, editText.getText().toString());
                        editor.putBoolean(KEY_PREFS_IS_AUTH, true);
                        editor.apply();
                        loadFragment(PersonalAccountFragmentController.newInstance());
                    }
                } else {
                    Toast.makeText(getContext(), "Неправильный логин или пароль!", Toast.LENGTH_LONG).show();
                }
            } else Toast.makeText(getContext(), "Введите данные!", Toast.LENGTH_LONG).show();

        });

        googleAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = AccountManager.newChooseAccountIntent(null, null, new String[]{"com.google"},
                        false, null, null, null, null);
                startActivityForResult(intent, 999);
            }
        });
        btnRegister.setOnClickListener(view1 -> {
            loadFragment(RegistrationFragment.newInstance());
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void onActivityResult(final int requestCode, final int resultCode,
                                 final Intent data) {
        if (requestCode == 999 && resultCode == RESULT_OK) {
            String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
            List<Account> foundAccounts = aDAO.getAccountsByLogin(accountName);
            if (foundAccounts.size() > 0) {
                editor.putString(KEY_PREFS_LOGIN, accountName);
                editor.putBoolean(KEY_PREFS_IS_AUTH, true);
                editor.apply();
                loadFragment(PersonalAccountFragmentController.newInstance());
            } else {
                editor.putString(KEY_PREFS_LOGIN, accountName);
                editor.putBoolean(KEY_PREFS_IS_AUTH, true);
                editor.apply();
                aDAO.insert(new Account(aDAO.getAllAccounts().size() + 1, accountName, new Random().ints(10, 48, 123).mapToObj(i -> String.valueOf((char) i)).collect(Collectors.joining()), 200));

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private boolean isValidForms(){
        return !editText.getText().toString().equals("") && !editText1.getText().toString().equals("");
    }


    public void loadFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_content, fragment);
        fragmentTransaction.commit();
    }
}
