package com.example.garbage_map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.garbage_map.entities.Account;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.AccountViewHolder> {
    private List<Account> mAccounts = new ArrayList<>();
    private Context mContext;

        public CustomAdapter(Context context, List<Account> accounts) {
            mContext = context;
            mAccounts = accounts;
        }

        @Override
        public CustomAdapter.AccountViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, parent, false);
            AccountViewHolder viewHolder = new AccountViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(CustomAdapter.AccountViewHolder holder, int position) {
            holder.bindRestaurant(mAccounts.get(position));
        }

        @Override
        public int getItemCount() {
            return mAccounts.size();
        }


        public class AccountViewHolder extends RecyclerView.ViewHolder {
            CircleImageView circleImageView;
            TextView textView, textView1;

            private Context mContext;

            public AccountViewHolder(View itemView) {
                super(itemView);
                mContext = itemView.getContext();
                circleImageView =itemView.findViewById(R.id.item_mage);
                textView = itemView.findViewById(R.id.score_item);
                textView1 = itemView.findViewById(R.id.usernameTextView);
            }

            public void bindRestaurant(Account account) {
               circleImageView.setImageResource(R.mipmap.ic_launcher_round);
               textView1.setText(account.getUsername());
               textView.setText(Integer.toString(account.getScore()));
            }
        }
    }

